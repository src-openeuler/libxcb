Name:           libxcb
Version:        1.16
Release:        1
Summary:        A C binding to the X11 protocol
License:        MIT
URL:            https://xcb.freedesktop.org/
Source0:        https://xcb.freedesktop.org/dist/%{name}-%{version}.tar.xz

# This is stolen straight from the pthread-stubs source:
# http://cgit.freedesktop.org/xcb/pthread-stubs/blob/?id=6900598192bacf5fd9a34619b11328f746a5956d
Source1:        pthread-stubs.pc.in

BuildRequires:  doxygen graphviz libtool libxslt pkgconfig libXau-devel
BuildRequires:  xcb-proto xorg-x11-util-macros python3 python3-devel

%description
The X protocol C-language Binding (XCB) is a replacement for Xlib featuring a
small footprint, latency hiding, direct access to the protocol, improved
threading support, and extensibility.

%package        devel
Summary:        Development files for %{name}
Requires:       %{name} = %{version}-%{release}

%description    devel
The %{name}-devel package contains development files for %{name}.

%package        help
Summary:        Documents for %{name}
Buildarch:      noarch
Requires:       man info
Provides:       %{name}-doc
Obsoletes:      %{name}-doc

%description    help
Man pages and other related documents for %{name}. 

%prep
%autosetup -n %{name}-%{version} -p1

%build
sed -i 's/pthread-stubs //' configure.ac
autoreconf -ivf
%configure --docdir=%{_docdir}/%{name}-%{version} --enable-selinux \
           --enable-xkb --enable-xinput --disable-xprint --disable-silent-rules \
	   --enable-unit-tests --disable-static

%disable_rpath
%make_build

%check
make check

%install
%make_install
install -pm 644 COPYING NEWS README.md $RPM_BUILD_ROOT%{_docdir}/%{name}-%{version}
sed 's,@libdir@,%{_libdir},;s,@prefix@,%{_prefix},;s,@exec_prefix@,%{_exec_prefix},' %{SOURCE1}  > $RPM_BUILD_ROOT%{_libdir}/pkgconfig/pthread-stubs.pc


%delete_la
%ldconfig_scriptlets

%files
%defattr(-,root,root)
%{_docdir}/%{name}-%{version}/COPYING
%{_libdir}/libxcb*.so.*

%files          devel
%defattr(-,root,root)
%{_includedir}/xcb
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files          help
%defattr(-,root,root)
%{_mandir}/man3/*.3*
%{_docdir}/%{name}-%{version}/* 
%exclude %{_docdir}/%{name}-%{version}/COPYING

%changelog
* Thu Dec 28 2023 Paul Thomas <paulthomas100199@gmail.com> - 1.16-1
- update to version 1.16

* Thu Oct 27 2022 wangkerong <wangkerong@h-partners.com> - 1.15-1
- upgrade to 1.15

* Sun Apr 24 2022 yangcheng <yangcheng87@h-partners.com> - 1.14-2
- enable check while building

* Fri Jul 31 2020 chxssg<chxssg@qq.com> - 1.14-1
- update package to 1.14 

* Tue Sep 03 2019 openEuler Buildteam <buildteam@openeuler.org> - 1.13.1-2
- Package init
